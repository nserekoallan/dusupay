const { stringify } = require('jade/lib/utils');
var mongoose = require('mongoose');

async () => {
    try{
        await mongoose.connect('mongodb://localhost:27017/donate',);
    }
    catch(err){
        console.error(err);
    }
}

var donationSchema = new mongoose.Schema({
    first_name: {
        type: String,
        required: true,
    },
    last_name: {
        type: String,
        required: true,
    },

    email: {
        type: String,
        required:true,
    },

    amount: {
        type: Number,
        required: true,
    },

    reference: {
        type: String,
        required: true,
    },

    createdOn: {
        type: Date,
        default: Date.now()
        }

    
});

var Donation = mongoose.model('Donation', donationSchema);

module.exports = {Donation};