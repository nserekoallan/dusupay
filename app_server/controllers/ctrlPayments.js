const dusuPublicKey = process.env.DUSU_PUBLIC_KEY;
const dusuSecretKey = process.env.DUSU_SECRET_KEY;

const req = require('express/lib/request');
var request = require('request-promise');
// var bodyParser = require("body-parser");
const _ = require('lodash');


module.exports.pay = (req, res)=>{
    var method = _.pick(req.body, ['method']);
    if(method === "CARD"){
        var form = _.pick(req.body, ['currency','amount','method','provider_id','account_number']);
        form.api_key = dusuPublicKey;
        form.merchant_reference = Math.random().toString(36).substr(5);
        form.narration = "Donation";
        form.redirect_url = "https://dusupaymentstest/paid";

        form = JSON.stringify(form);

        var requestOptions = {
            uri : "https://sandbox.dusupay.com/v1/collections",
            method: "POST",
            json : form,
            headers:{
                "content-type" : "application/json",
            }
            };
        request.post(requestOptions,
            function(err, response, body){
                if(err){
                    res.write("error");
                    console.error(err);
                } else {
                    response = JSON.parse(body);
                    res.redirect(response.data.payment_url);
                }
            }
        );
    } else {
        var form = _.pick(req.body,['currency','amount','method']);
        form.api_key = dusuPublicKey;
        form.merchant_reference = Math.random().toString(36).substr(5);
        form.narration = "Donation";

        form = JSON.stringify(form);
        
        var requestOptions = {
            uri : "https://sandbox.dusupay.com/v1/collections",
            method: "POST",
            json : form,
            headers:{
                "content-type" : "application/json",
            }
            };
            
        request.post(requestOptions,
            function(err, body){
                if(err){
                    res.write("error");
                    console.error(err);
                } else {
                    // res.write("success");
                    console.log("transaction successful")
                   
                    // console.log(response);
                }
            }
        );
    }
    
}


