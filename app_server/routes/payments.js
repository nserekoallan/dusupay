var express = require('express');
var router = express.Router();
var ctrl = require('../controllers/ctrlPayments');

router.post('/payments',ctrlPayments.pay);
router.get('/payments/paid', ctrlPayments.callBack);

module.exports = router;